import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from './router'
import store from './store'

import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueAxios, axios)

new Vue({
  router,
  store,
  render: h => h(App)

  // mounted: function () {
  //   axios.get('http://localhost:8080/trainerInnen')
  //     .then(response => console.log(response));
  // }
}).$mount('#app')

//  new Vue({
//   render: h => h(App),
// router
// }).$mount('#app')
