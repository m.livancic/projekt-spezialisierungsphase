package com.example.demo.dtos;

import com.example.demo.enums.FachEnums;

public class FachDTO {
    private String description;
    private FachEnums fachEnums;

    public FachDTO(String description, FachEnums fachEnums) {
        this.description = description;
        this.fachEnums = fachEnums;
    }

    // getter und setter


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FachEnums getFachEnums() {
        return fachEnums;
    }

    public void setFachEnums(FachEnums fachEnums) {
        this.fachEnums = fachEnums;
    }
}
