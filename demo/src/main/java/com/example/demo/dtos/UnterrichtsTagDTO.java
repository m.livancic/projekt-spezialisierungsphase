package com.example.demo.dtos;

import com.example.demo.enums.TageEnums;

public class UnterrichtsTagDTO {
    private TageEnums tageEnums;
    private boolean qualifying;
    private int trainerId;
    private int fachId;
    private int gruppeId;

    public UnterrichtsTagDTO(){}

    public UnterrichtsTagDTO(TageEnums tageEnums, boolean qualifying, int trainerId, int fachId, int gruppeId) {
        this.tageEnums = tageEnums;
        this.qualifying = qualifying;
        this.trainerId = trainerId;
        this.fachId = fachId;
        this.gruppeId = gruppeId;


    }

    // getter und setter
    public TageEnums getTageEnums() {
        return tageEnums;
    }

    public void setTageEnums(TageEnums tageEnums) {
        this.tageEnums = tageEnums;
    }

    public boolean isQualifying() {
        return qualifying;
    }

    public void setQualifying(boolean qualifying) {
        this.qualifying = qualifying;
    }

    public int getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(int trainerId) {
        this.trainerId = trainerId;
    }

    public int getFachId() {
        return fachId;
    }

    public void setFachId(int fachId) {
        this.fachId = fachId;
    }

    public int getGruppeId() {
        return gruppeId;
    }

    public void setGruppeId(int gruppeId) {
        this.gruppeId = gruppeId;
    }
}
