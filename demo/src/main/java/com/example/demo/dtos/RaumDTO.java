package com.example.demo.dtos;

public class RaumDTO {
    private String name;
    private int nummer;

    public RaumDTO(String name, int nummer) {
        this.name = name;
        this.nummer = nummer;
    }

    // getter und setter


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }


}
