package com.example.demo.dtos;

import com.example.demo.enums.AnstellungEnums;

public class TrainerInnenDTO {
    private String name;
    private String eMail;
    private String adresse;
    private String telnum;

    private AnstellungEnums anstellungEnums;
    private float bruttoGehalt;
    private double nettoGehalt;
    private int fachId;


    public TrainerInnenDTO(String name, String eMail, String adresse, String telnum, AnstellungEnums anstellungEnums, float bruttoGehalt, double nettoGehalt, int fachId) {
        this.name = name;
        this.eMail = eMail;
        this.adresse = adresse;
        this.telnum = telnum;
        this.anstellungEnums = anstellungEnums;
        this.bruttoGehalt = bruttoGehalt;
        this.nettoGehalt = nettoGehalt;
        this.fachId = fachId;
    }

    // getter und setter


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelnum() {
        return telnum;
    }

    public void setTelnum(String telnum) {
        this.telnum = telnum;
    }

    public AnstellungEnums getAnstellungEnums() {
        return anstellungEnums;
    }

    public void setAnstellungEnums(AnstellungEnums anstellungEnums) {
        this.anstellungEnums = anstellungEnums;
    }

    public float getBruttoGehalt() {
        return bruttoGehalt;
    }

    public void setBruttoGehalt(float bruttoGehalt) {
        this.bruttoGehalt = bruttoGehalt;
    }

    public int getFachId() {
        return fachId;
    }

    public void setFachId(int fachId) {
        this.fachId = fachId;
    }

    public double getNettoGehalt() {
        return nettoGehalt;
    }

    public void setNettoGehalt(double nettoGehalt) {
        this.nettoGehalt = nettoGehalt;
    }
}
