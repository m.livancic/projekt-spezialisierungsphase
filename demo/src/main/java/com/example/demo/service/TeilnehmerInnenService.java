package com.example.demo.service;

import com.example.demo.dtos.TeilnehmerInnenDTO;
import com.example.demo.model.Gruppe;
import com.example.demo.model.TeilnehmerInnen;
import com.example.demo.repositories.GruppeCRUDRepository;
import com.example.demo.repositories.TeilnehmerInnenCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeilnehmerInnenService {

    @Autowired
    private TeilnehmerInnenCRUDRepository teilnehmerInnenCRUDRepository;

    @Autowired
    private GruppeCRUDRepository gruppeCRUDRepository;



    public List<TeilnehmerInnen> getTeilnehmerInnen() {
        return (List<TeilnehmerInnen>) teilnehmerInnenCRUDRepository.findAll();
    }

    public TeilnehmerInnen getTeilnehmerInnenbyId(Integer teilnehmerInnenId) {
       return (TeilnehmerInnen) teilnehmerInnenCRUDRepository.findById(teilnehmerInnenId).get();
    }

    public void setTeilnehmerInnen(String name, String eMail, String adresse, String telnum, String startdatum, String enddatum, Integer gruppeId) {
        Gruppe gruppe =  gruppeCRUDRepository.findById(gruppeId).get();

        TeilnehmerInnen teilnehmerInnen = new TeilnehmerInnen(name, eMail, adresse, telnum, startdatum, enddatum, gruppe);

        gruppe.setTeilnehmerInnen(teilnehmerInnen);

        teilnehmerInnenCRUDRepository.save(teilnehmerInnen);
        gruppeCRUDRepository.save(gruppe);
    }


    public void neueTeilnehmerIn(TeilnehmerInnen teilnehmerInnen) {
        teilnehmerInnenCRUDRepository.save(teilnehmerInnen);
    }

    public void teilnehmerInLoeschen(Integer teilnehmerInnenId) {
        boolean exists = teilnehmerInnenCRUDRepository.existsById(teilnehmerInnenId);
        if(!exists) {
            throw new IllegalStateException("TeilnehmerIn mit id " + teilnehmerInnenId + " existiert nicht");
        }teilnehmerInnenCRUDRepository.deleteById(teilnehmerInnenId);
    }

    public void teilnehmerInUpdate(TeilnehmerInnenDTO teilnehmerInnenDTO, Integer teilnehmerInnenId) {
        TeilnehmerInnen teilnehmerInnendb = teilnehmerInnenCRUDRepository.findById(teilnehmerInnenId).get();

        if (teilnehmerInnenDTO.getStartdatum() != null)
            teilnehmerInnendb.setStartdatum(teilnehmerInnenDTO.getStartdatum());
        if (teilnehmerInnenDTO.getName() != null)
            teilnehmerInnendb.setName(teilnehmerInnenDTO.getName());
        if (teilnehmerInnenDTO.getAdresse() != null)
            teilnehmerInnendb.setAdresse(teilnehmerInnenDTO.getAdresse());
        if (teilnehmerInnenDTO.getEnddatum() != null)
            teilnehmerInnendb.setEnddatum(teilnehmerInnenDTO.getEnddatum());
        if (teilnehmerInnenDTO.geteMail() != null)
            teilnehmerInnendb.seteMail(teilnehmerInnenDTO.geteMail());
        if (teilnehmerInnenDTO.getTelnum() != null)
            teilnehmerInnendb.setTelnum(teilnehmerInnenDTO.getTelnum());

        if (teilnehmerInnenDTO.getGruppeId() != 0)
            if (teilnehmerInnendb.getGruppe() != null)
                teilnehmerInnendb.getGruppe().gruppeLoeschen(teilnehmerInnendb);
        Gruppe gruppe = gruppeCRUDRepository.findById(teilnehmerInnenDTO.getGruppeId()).get();
        teilnehmerInnendb.setGruppe(gruppe);
        gruppe.setTeilnehmerInnen(teilnehmerInnendb);
        gruppeCRUDRepository.save(gruppe);


        teilnehmerInnenCRUDRepository.save(teilnehmerInnendb);
    }


}
