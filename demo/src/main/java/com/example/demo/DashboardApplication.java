package com.example.demo;

import com.example.demo.dtos.TrainerInnenDTO;
import com.example.demo.enums.AnstellungEnums;
import com.example.demo.enums.FachEnums;
import com.example.demo.enums.GruppenEnums;
import com.example.demo.model.*;
import com.example.demo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@SpringBootApplication
public class DashboardApplication implements CommandLineRunner {

	@Autowired
	TeilnehmerInnenCRUDRepository teilnehmerInnenCRUDRepository;

	@Autowired
	TrainerInnenCRUDRepository trainerInnenCRUDRepository;

	@Autowired
	GruppeCRUDRepository gruppeCRUDRepository;

	@Autowired
	FachCRUDRepository fachCRUDRepository;

	@Autowired
	UnterrichtsTagCRUDRepository unterrichtsTagCRUDRepository;

	@Autowired
	RaumCRUDRepository raumCRUDRepository;

	@Autowired
	AccountCrud accountRepository;

	@Autowired
	PasswordEncoder passwordEncoder;


	List<Gruppe> gruppeList = new ArrayList<>();
	List<TeilnehmerInnen> teilnehmerInnenList = new ArrayList<>();

	List<Gruppe> fachList = new ArrayList<>();
	List<TrainerInnen> trainerInnenList = new ArrayList<>();


	public static void main(String[] args) {
		SpringApplication.run(DashboardApplication.class, args);

	}


	@Override
	public void run(String... args) throws Exception {

		accountRepository.save(new Account("Marko", passwordEncoder.encode("1234"), "marko@ada.com", ProjectRoles.ADMIN));
		accountRepository.save(new Account("Anna", passwordEncoder.encode("123"), "ana@anna.com", ProjectRoles.USER));
		accountRepository.save(new Account("Tom", passwordEncoder.encode("1111"), "tom@tom.com", ProjectRoles.USER));


		for (int i = 0; i < 9; i++) {
			String vorname = "";
			String nachname = "";
			String adresse = "";
			String telnum;
			String descriptionJava = "";
			String descriptionWeb = "";
			String raumName = "";


			int zufallNummer = (int) (Math.random() * 8);


			int zufallVorname = (int) (Math.random() * 11);
			int zufallNachname = (int) (Math.random() * 11);
			int zufallAdresse = (int) (Math.random() * 10);
			int zufallTelnum = (int) (Math.random() * 1000000);
			int zufallRaumName = (int) (Math.random() * 9);

			int zufallDescriptionJava = (int) (Math.random() * 5);
			int zufallDescriptionWeb = (int) (Math.random() * 5);

			switch (zufallRaumName) {
				case 0:
					raumName = "Alan Turing";
					break;
				case 1:
					raumName = "James Gossling";
					break;
				case 2:
					raumName = "Bjarne Stoustrup";
					break;
				case 3:
					raumName = " Dennis Ritchie";
					break;
				case 4:
					raumName = "Guido van Rossum";
					break;
				case 5:
					raumName = "Ada Lovelace";
					break;
				case 6:
					raumName = "Grace Hopper ";
					break;
				case 7:
					raumName = "Adele Goldberg";
					break;
				case 8:
					raumName = "Joan Clarke";
					break;
			}


			switch (zufallDescriptionJava) {
				case 0:
					descriptionJava = "Spring Boot";
					break;

				case 1:
					descriptionJava = "How to CRUD";
					break;
				case 2:
					descriptionJava = "Java Klassen";
					break;
				case 3:
					descriptionJava = "Annotations";
					break;
				case 4:
					descriptionJava = "Schleifen";
					break;
			}

			switch (zufallDescriptionWeb) {
				case 0:
					descriptionWeb = "Bootstrap";
					break;

				case 1:
					descriptionWeb = "VueJs";
					break;
				case 2:
					descriptionWeb = "Javascript ";
					break;
				case 3:
					descriptionWeb = "HTML Grundlagen";
					break;
				case 4:
					descriptionWeb = "CSS Grundlagen";
					break;
			}

			switch (zufallVorname) {
				case 0:
					vorname = "Nadine";
					break;
				case 1:
					vorname = "Simon";
					break;
				case 2:
					vorname = "Monika";
					break;
				case 3:
					vorname = "Richard";
					break;
				case 4:
					vorname = "Sarah";
					break;
				case 5:
					vorname = "Kasun";
					break;
				case 6:
					vorname = "Laura";
					break;
				case 7:
					vorname = "Sebastian";
					break;
				case 8:
					vorname = "Susanne";
					break;
				case 9:
					vorname = "Mia";
					break;
				case 10:
					vorname = "Thomas";
					break;
			}

			switch (zufallNachname) {
				case 0:
					nachname = "Mustermann";
					break;
				case 1:
					nachname = "Musterfrau";
					break;
				case 2:
					nachname = "Müller";
					break;
				case 3:
					nachname = "Rosenrot";
					break;
				case 4:
					nachname = "Bauer";
					break;
				case 5:
					nachname = "Weber";
					break;
				case 6:
					nachname = "Hoffmann";
					break;
				case 7:
					nachname = "Hemmer";
					break;
				case 8:
					nachname = "Leithner";
					break;
				case 9:
					nachname = "Brunner";
					break;
				case 10:
					nachname = "Vogel";
					break;
			}

			switch (zufallAdresse) {
				case 0:
					adresse = "Haupt" + "straße" + " " + "1" + "," + "1100 Wien";
					break;
				case 1:
					adresse = "Wiener" + "straße" + " " + "15" + "," + "1150 Wien";
					break;
				case 2:
					adresse = "Garten" + "straße" + " " + "35" + "," + "1230 Wien";
					break;
				case 3:
					adresse = "Dorf" + "straße" + " " + "38" + "," + "1140 Wien";
					break;
				case 4:
					adresse = "Bahnhof" + "straße" + " " + "3" + "," + "1200 Wien";
					break;
				case 5:
					adresse = "Kirch" + "straße" + " " + "1" + "," + "1160 Wien";
					break;
				case 6:
					adresse = "Amsel" + "weg" + " " + "9" + "," + "1080 Wien";
					break;
				case 7:
					adresse = "Wiesen" + "weg" + " " + "20" + "," + "1090 Wien";
					break;
				case 8:
					adresse = "Feld" + "straße" + " " + "11" + "," + "1130 Wien";
					break;
				case 9:
					adresse = "Berg" + "straße" + " " + "133" + "," + "1110 Wien";
					break;
			}

			adresse += " ";
			telnum = "+436" + zufallTelnum;
			String name = vorname + " " + nachname;
			String eMail = vorname.toLowerCase() + "." + nachname.toLowerCase() + "@outlook.com";

			descriptionJava += " ";
			descriptionWeb+= " ";
			raumName += " ";


			try {
				teilnehmerInnenCRUDRepository.save(new TeilnehmerInnen(name, eMail, adresse, telnum, "2022-10-10", "2023-03-10"));
				fachCRUDRepository.save(new Fach(descriptionJava, FachEnums.JAVA));
				fachCRUDRepository.save(new Fach(descriptionWeb, FachEnums.WEB));
				raumCRUDRepository.save(new Raum(raumName, zufallNummer));
				//trainerInnenDTOCRUDRepository.save(new TrainerInnenDTO(name, eMail, adresse, telnum, AnstellungEnums.VOLLZEIT, 2750, 6));
//				teilnehmerInnenList.add(teilnehmerInnen);
//				trainerInnenCRUDRepository.save(new TrainerInnen(name, eMail, adresse, telnum, AnstellungEnums.VOLLZEIT, 2500));
//				gruppeCRUDRepository.save(new Gruppe(zufallNummer, GruppenEnums.SW, Boolean.FALSE));

			} catch (Exception e) {
				System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage());
			}


		}

	}
}







