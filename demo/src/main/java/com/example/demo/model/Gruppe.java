package com.example.demo.model;

import com.example.demo.enums.GruppenEnums;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "gruppe")
public class Gruppe {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int gruppen_ID;

    @Column
    private int nummer;

    @Enumerated(EnumType.STRING)
    private GruppenEnums gruppenEnums;

    @Column
    Boolean qualifying;

    @OneToOne
    @JsonManagedReference(value="grurau")
    private Raum raum;

    @OneToMany(mappedBy = "gruppe")
    @JsonManagedReference(value="gruppe")
    private Set<TeilnehmerInnen> teilnehmerInnenSet = new HashSet<>();


    @OneToMany(mappedBy = "gruppe")
    @JsonBackReference(value ="gruunt")
    private Set<UnterrichtsTag> unterrichtsTagSet = new HashSet<>();

    public Gruppe(){}

    public Gruppe(int nummer, GruppenEnums gruppenEnums, Boolean qualifying){
        this.nummer = nummer;
        this.gruppenEnums = gruppenEnums;
        this.qualifying = qualifying;
    }

    public Gruppe(int nummer, GruppenEnums gruppenEnums, Raum raum){
        this.nummer = nummer;
        this.gruppenEnums = gruppenEnums;
        this.raum = raum;
    }

    public Gruppe(int nummer, GruppenEnums gruppenEnums, Boolean qualifying, Raum raum) {
        this.nummer = nummer;
        this.gruppenEnums = gruppenEnums;
        this.qualifying = qualifying;
        this.raum = raum;
    }


    //getter und setter


    public int getGruppen_ID() {
        return gruppen_ID;
    }

    public void setGruppen_ID(int gruppen_ID) {
        this.gruppen_ID = gruppen_ID;
    }

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    public GruppenEnums getGruppenEnums() {
        return gruppenEnums;
    }

    public void setGruppenEnums(GruppenEnums gruppenEnums) {
        this.gruppenEnums = gruppenEnums;
    }

    public Raum getRaum() {
        return raum;
    }

    public void setRaum(Raum raum) {
        this.raum = raum;
    }

    public Set<TeilnehmerInnen> getTeilnehmerInnenSet() {
        return teilnehmerInnenSet;
    }

    public void setTeilnehmerInnenSet(Set<TeilnehmerInnen> teilnehmerInnenSet) {
        this.teilnehmerInnenSet = teilnehmerInnenSet;
    }

    public Set<UnterrichtsTag> getUnterrichtsTagSet() {
        return unterrichtsTagSet;
    }

    public void setUnterichtsTag(UnterrichtsTag unterrichtsTag) {
        this.unterrichtsTagSet.add(unterrichtsTag);
    }

    public void setTeilnehmerInnen(TeilnehmerInnen teilnehmerInnen) {
        this.teilnehmerInnenSet.add(teilnehmerInnen);
    }

    public void setGruppe(Gruppe gruppe) {

    }

    public void unterrichtLoeschen(UnterrichtsTag unterrichtsTag) {
        unterrichtsTagSet.remove(unterrichtsTag);
    }

    public void gruppeLoeschen(TeilnehmerInnen teilnehmerInnen) {
        teilnehmerInnenSet.remove(teilnehmerInnen);
    }

    public void gruppeRaumLoeschen(Raum raum) {
        this.raum = null;
    }

    public Boolean getQualifying() {
        return qualifying;
    }

    public void setQualifying(Boolean qualifying) {
        this.qualifying = qualifying;
    }
}

