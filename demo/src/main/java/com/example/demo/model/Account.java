package com.example.demo.model;

import javax.annotation.processing.Generated;
import javax.persistence.*;

@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private String eMail;

    @Column
    private ProjectRoles userrole;

    public Account(){}

    public Account(String username, String password, String eMail, ProjectRoles userrole) {
        this.username = username;
        this.password = password;
        this.eMail = eMail;
        this.userrole = userrole;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;

    }

    public ProjectRoles getUserrole() {
        return userrole;
    }

    public void setUserrole(ProjectRoles userrole) {
        this.userrole = userrole;
    }
}
