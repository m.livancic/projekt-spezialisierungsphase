package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Raum {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int raum_id;

    @Column(unique = true)
    private String name;

    @Column
    private int nummer;

    @OneToOne
    @JsonBackReference(value="grurau")
    @JoinColumn(name = "gruppe_id")
    private Gruppe gruppe;

    public Raum(){}

    public Raum(String name, int nummer){
        this.name = name;
        this.nummer = nummer;
    }

    public Raum(String name, int nummer, Gruppe gruppe){
        this.name = name;
        this.nummer = nummer;
        this.gruppe = gruppe;
    }

    // getter und setter


    public int getRaum_id() {
        return raum_id;
    }

    public void setRaum_id(int raum_id) {
        this.raum_id = raum_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    public Gruppe getGruppe() {
        return gruppe;
    }

    public void setGruppe(Gruppe gruppe) {
        this.gruppe = gruppe;
    }

    public void gruppeLoeschen(Gruppe gruppe) {
        this.gruppe = null;
    }
}
