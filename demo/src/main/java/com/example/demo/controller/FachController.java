package com.example.demo.controller;

import com.example.demo.dtos.FachDTO;
import com.example.demo.model.Fach;
import com.example.demo.service.FachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/fach")
public class FachController {

    @Autowired
    private FachService fachService;

    public FachController(FachService fachService) {
        this.fachService = fachService;
    }

    @CrossOrigin
    @GetMapping
    public List<Fach> getFach() {
        return fachService.getFach();
    }

    @CrossOrigin
    @PostMapping
    public void neuesFach (@RequestBody FachDTO fachDTO) {
        fachService.setFach(fachDTO.getDescription(), fachDTO.getFachEnums());
    }

    @CrossOrigin
    @DeleteMapping(path = "{fachId}")
    public void fachLoeschen(@PathVariable("fachId") Integer fachId) {
        fachService.fachLoeschen(fachId);
    }

    @CrossOrigin
    @PutMapping(path = "{fachId}")
    public void fachUpdate(@RequestBody FachDTO fachDTO, @PathVariable("fachId") Integer fachId)  {
        fachService.fachUpdate(fachDTO, fachId);
    }

}
