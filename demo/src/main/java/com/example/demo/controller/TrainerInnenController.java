package com.example.demo.controller;

import com.example.demo.dtos.TrainerInnenDTO;
import com.example.demo.enums.AnstellungEnums;
import com.example.demo.model.TeilnehmerInnen;
import com.example.demo.model.TrainerInnen;
import com.example.demo.service.TrainerInnenService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping (path = "/trainerInnen")
public class TrainerInnenController {

    private TrainerInnenService trainerInnenService;

    public TrainerInnenController (TrainerInnenService trainerInnenService) {
        this.trainerInnenService = trainerInnenService;
    }

    @CrossOrigin
    @GetMapping
    public List<TrainerInnen> getTrainerInnen() {
        return trainerInnenService.getTrainerInnen();
    }

    @CrossOrigin
    @GetMapping(path ="{trainerInnenId}")
    public TrainerInnen getTrainnerInnenbyId(@PathVariable("trainerInnenId") Integer trainerInnenId) {
        return trainerInnenService.getTrainerInnenbyId(trainerInnenId);
    }


    @CrossOrigin
    @PostMapping
    public void trainerInNeu (@RequestBody TrainerInnenDTO trainerInnenDTO) {
        trainerInnenService.setTrainnerinnen(trainerInnenDTO.getName(), trainerInnenDTO.geteMail(), trainerInnenDTO.getAdresse(), trainerInnenDTO.getTelnum(), trainerInnenDTO.getAnstellungEnums(), trainerInnenDTO.getBruttoGehalt(), trainerInnenDTO.getFachId());
    }

    @CrossOrigin
    @DeleteMapping(path = "{trainerInnenId}")
    public void trainerInloeschen(@PathVariable("trainerInnenId") Integer trainerInnenId) {
        trainerInnenService.trainerInloeschen(trainerInnenId);
    }

    @CrossOrigin
    @PutMapping(path = "{trainerInnenId}")
    public void trainerInUpdate(
            @RequestBody TrainerInnenDTO trainerInnenDTO, @PathVariable("trainerInnenId") Integer trainerInnenId) {
        trainerInnenService.trainerInUpdate(trainerInnenDTO, trainerInnenId);
    }


}
