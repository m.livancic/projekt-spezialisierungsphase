package com.example.demo.controller;

import com.example.demo.dtos.GruppeDTO;
import com.example.demo.model.Gruppe;
import com.example.demo.model.TeilnehmerInnen;
import com.example.demo.service.GruppeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/gruppe")
public class GruppeController {

    @Autowired
    private GruppeService gruppeService;

    public GruppeController(GruppeService gruppeService) {
        this.gruppeService = gruppeService;
    }

    @CrossOrigin
    @GetMapping
    public List<Gruppe> getGruppe() {
        return gruppeService.getGruppe();
    }

    @CrossOrigin
    @GetMapping(path="{gruppenId}")
    public Gruppe getGruppeById(@PathVariable("gruppenId") Integer gruppenId) {
        return gruppeService.getGruppeById(gruppenId);
    }


    @CrossOrigin
    @PostMapping
    public void gruppeNeu(@RequestBody GruppeDTO gruppeDTO) {
        gruppeService.setGruppe(gruppeDTO.getNummer(), gruppeDTO.getGruppenEnums(), gruppeDTO.getQualifying(),  gruppeDTO.getRaumId());
    }

    @CrossOrigin
    @DeleteMapping(path ="{gruppeId}")
    public void gruppeLoeschen(@PathVariable("gruppeId") Integer gruppeId) {
        gruppeService.gruppeLoeschen(gruppeId);
    }

    @CrossOrigin
    @PutMapping(path = "{gruppeId}")
    public void gruppeUpdate(@RequestBody GruppeDTO gruppeDTO, @PathVariable("gruppeId") Integer gruppeId) {
        gruppeService.gruppeUpdate(gruppeDTO, gruppeId);
    }
}
