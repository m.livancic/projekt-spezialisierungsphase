package com.example.demo.controller;

import com.example.demo.dtos.TeilnehmerInnenDTO;
import com.example.demo.model.TeilnehmerInnen;
import com.example.demo.service.TeilnehmerInnenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/teilnehmerInnen")
public class TeilnehmerInnenController {

    @Autowired
    private  TeilnehmerInnenService teilnehmerInnenService;

    public TeilnehmerInnenController(TeilnehmerInnenService teilnehmerInnenService) {
        this.teilnehmerInnenService = teilnehmerInnenService;
    }

    @CrossOrigin
    @GetMapping
    public List<TeilnehmerInnen> getTeilnehmerInnen() {
        return teilnehmerInnenService.getTeilnehmerInnen();
    }


    @CrossOrigin
    @GetMapping(path = "{teilnehmerInnenId}")
    public TeilnehmerInnen getTeilnehmerInnenbyId(@PathVariable ("teilnehmerInnenId") Integer teilnehmerInnenId) {
        return teilnehmerInnenService.getTeilnehmerInnenbyId(teilnehmerInnenId);
    }
    @CrossOrigin
    @PostMapping
    public void teilnehmerInNeu (@RequestBody TeilnehmerInnenDTO teilnehmerInnenDTO) {
        teilnehmerInnenService.setTeilnehmerInnen(teilnehmerInnenDTO.getName(), teilnehmerInnenDTO.geteMail(),
                teilnehmerInnenDTO.getAdresse(), teilnehmerInnenDTO.getTelnum(), teilnehmerInnenDTO.getStartdatum(), teilnehmerInnenDTO.getEnddatum(), teilnehmerInnenDTO.getGruppeId());
    }


    @CrossOrigin
    @DeleteMapping(path = "{teilnehmerInnenId}")
    public void teilnehmerInloeschen(@PathVariable("teilnehmerInnenId") Integer teilnehmerInnenId) {
        teilnehmerInnenService.teilnehmerInLoeschen(teilnehmerInnenId);
    }

    @CrossOrigin
    @PutMapping(path = "{teilnehmerInnenId}")
    public void teilnehmerInUpdate(
            @RequestBody TeilnehmerInnenDTO teilnehmerInnenDTO, @PathVariable("teilnehmerInnenId") Integer teilnehmerInnenId) {
        teilnehmerInnenService.teilnehmerInUpdate(teilnehmerInnenDTO, teilnehmerInnenId);
    }

}

