package com.example.demo.controller;

import com.example.demo.dtos.RaumDTO;
import com.example.demo.model.Raum;
import com.example.demo.model.TeilnehmerInnen;
import com.example.demo.service.RaumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/raum")
public class RaumController {

    @Autowired
    private RaumService raumService;

    public RaumController(RaumService raumService) {
        this.raumService = raumService;
    }

    @CrossOrigin
    @GetMapping
    public List<Raum> getRaum() {
        return raumService.getRaum();
    }


    @CrossOrigin
    @PostMapping
    public void raumNeu(@RequestBody RaumDTO raumDTO) {
        raumService.setRaum(raumDTO.getName(), raumDTO.getNummer()/*, raumDTO.getGruppeId()*/);
    }

    @CrossOrigin
    @DeleteMapping(path = "{raumId}")
    public void raumLoeschen(@PathVariable("raumId") Integer raumId) {
        raumService.raumLoeschen(raumId);
    }

    @CrossOrigin
    @PutMapping(path = "{raumId}")
    public void raumUpdate(@RequestBody RaumDTO raumDTO, @PathVariable("raumId") Integer raumId) {
        raumService.raumUpdate(raumDTO, raumId);
    }


}
