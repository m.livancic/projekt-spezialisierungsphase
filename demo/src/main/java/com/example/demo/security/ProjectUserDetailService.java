package com.example.demo.security;

import com.example.demo.model.Account;
import com.example.demo.repositories.AccountCrud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;

@Service
@Transactional
public class ProjectUserDetailService implements UserDetailsService {

    @Autowired
    AccountCrud accountCrud;

    //loadUserByUsername("Marko");
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Account  a = accountCrud.findOneByUsername(username);

        if(a==null){
            throw new UsernameNotFoundException("User with name " + username + " does not exist");
        }

        ArrayList<GrantedAuthority> authorities = new ArrayList<>();

        authorities.add(new SimpleGrantedAuthority("ROLE_" +a.getUserrole().toString()));


        User springSecUser = new User(a.getUsername(),a.getPassword(),authorities);

        return springSecUser;
    }
}
