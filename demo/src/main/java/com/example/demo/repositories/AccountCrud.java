package com.example.demo.repositories;

import com.example.demo.model.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountCrud extends CrudRepository<Account, Integer> {

    Account findOneByUsername(String name);
}
