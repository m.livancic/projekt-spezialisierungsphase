package com.example.demo.repositories;

import com.example.demo.model.UnterrichtsTag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnterrichtsTagCRUDRepository extends CrudRepository<UnterrichtsTag, Integer> {
}
